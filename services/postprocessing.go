package services

import (
	"enlabs/database"
	"enlabs/helpers"

	"github.com/mileusna/crontab"
)

// Cron ...
var Cron *crontab.Crontab

// CancelLastTen ...
func CancelLastTen() error {

	users := &[]User{}
	err := database.PostgreSQL.Find(&users).Error
	if err != nil {
		helpers.PrintErrorf("Failed to get users list: %s", err.Error())
		return err
	}

	for _, user := range *users {

		// Find last ten odd entries and mark them cancelled
		transactions := &[]Transaction{}
		err = database.PostgreSQL.Order("created_at desc").Limit(10).Where("user_id = ? AND id % 2 <> 0 AND cancelled = ?", user.ID, 0).Find(&transactions).Update("cancelled", 1).Error
		if err != nil {
			helpers.PrintErrorf("Failed to get transactions: %s", err.Error())
			return err
		}

		if len(*transactions) == 0 {
			helpers.PrintErrorf("No more entries to cancel in the database")
			return err
		}

		// Correct user's balance according to canceled entries
		for _, transaction := range *transactions {

			if transaction.State == "win" {
				user.Balance = user.Balance - transaction.Amount
			} else {
				user.Balance = user.Balance + transaction.Amount
			}
		}

		// Save updated user's balance to the database
		err = database.PostgreSQL.Model(user).Where("id = ?", user.ID).Update("balance", user.Balance).Error
		if err != nil {
			helpers.PrintErrorf("Updating user balance failed", err.Error())
			return err
		}

	}

	return err

}
