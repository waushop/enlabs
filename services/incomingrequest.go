package services

import (
	"enlabs/database"
	"enlabs/helpers"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
)

// User ...
type User struct {
	ID           int           `json:"id" gorm:"primary_key"`
	Username     string        `json:"username" gorm:"type:varchar(50) UNIQUE"`
	Balance      float64       `json:"balance" gorm:"type:decimal(10,2)"`
	Transactions []Transaction `json:"transactions" gorm:"-"`
}

// Transaction ...
type Transaction struct {
	ID        int     `json:"id" gorm:"primary_key"`
	State     string  `json:"state" gorm:"type:varchar(10)"`
	Amount    float64 `json:"amount" gorm:"type:decimal(10,2)"`
	UserID    int     `json:"user_id" gorm:"type:integer"`
	CreatedAt string  `json:"created_at" gorm:"type:varchar(20)"`
	Cancelled int     `json:"cancelled" gorm:"type:integer"`
}

// GetUsers ...
func GetUsers(c echo.Context) error {

	users := &[]User{}
	err := database.PostgreSQL.Find(&users).Error
	if err != nil {
		helpers.PrintErrorf("Failed to get users: %s", err.Error())
		return err
	}

	return c.JSON(http.StatusOK, users)
}

//GetUserByID ...
func GetUserByID(c echo.Context) error {

	id, _ := strconv.Atoi(c.Param("id"))

	user := &User{}
	err := database.PostgreSQL.First(user, id).Error
	if err != nil {
		helpers.PrintErrorf("Failed to get user: %s", err.Error())
		return err
	}

	transactions := &[]Transaction{}

	err = database.PostgreSQL.Where("user_id = ? AND cancelled = ?", user.ID, 0).Find(&transactions).Error
	if err != nil {
		helpers.PrintErrorf("Failed to get transactions: %s", err.Error())
		return err
	}

	user.Transactions = *transactions

	return c.JSON(http.StatusOK, user)
}

// UpdateBalance ...
func UpdateBalance(c echo.Context) error {

	transaction := &Transaction{}

	transaction.UserID, _ = strconv.Atoi(c.Param("id"))
	transaction.State = c.FormValue("state")
	transaction.Amount, _ = strconv.ParseFloat(c.FormValue("amount"), 64)

	currentTime := time.Now()
	transaction.CreatedAt = currentTime.Format("2006-01-02 15:04:05")

	user := &User{}
	err := database.PostgreSQL.Where("id = ?", transaction.UserID).Find(&user).Error
	if err != nil {
		helpers.PrintErrorf("Failed to get user: %s", err.Error())
		return err
	}

	// State win increases and lost decreases user's balance
	switch transaction.State {
	case "win":
		user.Balance = user.Balance + transaction.Amount
	case "lost":
		if user.Balance-transaction.Amount < 0 {
			return c.String(http.StatusOK, "Not enough funds on account to complete the transaction")
		}
		user.Balance = user.Balance - transaction.Amount
	default:
		return c.String(http.StatusOK, "Invalid state sent in request")
	}

	// Save incoming request to database
	err = database.PostgreSQL.Save(transaction).Error
	if err != nil {
		helpers.PrintErrorf("Failed to save transaction: %s", err.Error())
		return err
	}

	// Save updated user's balance to database
	err = database.PostgreSQL.Save(user).Error
	if err != nil {
		helpers.PrintErrorf("Failed to update balance: %s", err.Error())
		return err
	}

	transactions := &[]Transaction{}

	err = database.PostgreSQL.Where("user_id = ? AND cancelled = ?", user.ID, 0).Find(&transactions).Error
	if err != nil {
		helpers.PrintErrorf("Failed to get transactions: %s", err.Error())
		return err
	}

	user.Transactions = *transactions

	return c.JSON(http.StatusOK, user)

}
