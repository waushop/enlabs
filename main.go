package main

import (
	"encoding/gob"
	"enlabs/database"
	"enlabs/helpers"
	"enlabs/services"
	"net/http"

	"github.com/labstack/echo"
	"github.com/mileusna/crontab"
)

func init() {
	// Initialize database
	database.InitDatabaseConnection()

	// Automigrate
	database.PostgreSQL.AutoMigrate(&services.User{}, &services.Transaction{})

	// Default users
	database.PostgreSQL.FirstOrCreate(&services.User{Username: "Spiderman", Balance: 100})

	gob.Register(&services.User{})
}

func main() {

	// Start crontab
	ctab := crontab.New()

	cronInterval := "5"
	err := ctab.AddJob("*/"+cronInterval+" * * * *", services.CancelLastTen)
	if err != nil {
		helpers.PrintErrorf("No transactions found: %s", err.Error())
		return
	}

	// Echo instance
	e := echo.New()

	//e.Use(middleware.Logger())

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Enlabs Technical Task\n\n Endpoints:\n\n- GET: /users - Get all users\n- GET: /users/{id} - Get user by ID\n- POST: /users/{id}/update - For sending post requests by form-data {'state' : 'win/lost', 'amount' : '10.15 or something else'}\n\nOn initialization there is one demo user made called Spiderman (ID: 1) with starting balance of 100.")
	})

	e.GET("/users", services.GetUsers)
	e.GET("/users/:id", services.GetUserByID)
	e.POST("/users/:id/update", services.UpdateBalance)

	// Start server
	e.Logger.Fatal(e.Start(":6969"))
}
