package helpers

import "log"

// PrintErrorf ...
func PrintErrorf(format string, v ...interface{}) {
	go log.Printf("ERROR\t"+format, v)
}
