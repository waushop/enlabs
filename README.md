# Enlabs Technical Task
### 📖 Golang project for processing incoming http requests.
- By described GET endpoints in the lower part of this document you are able get all users and single user by it's ID. With a POST endpoint you can update user's balance by custom 'amount' weather it was a 'win' or 'lost' as a 'status'.

- Every 5 minutes all user's transactions with a odd ID are being flagged as canceled.

- On initialization there is one demo user made called Spiderman (ID: 1) with starting balance of 100.

## 🚀 Getting started

#### Install PostgreSQL [[postgresql.org](https://www.postgresql.org)] or Docker [[docker.com](https://www.docker.com/get-started)] on your host machine

#### Clone repository into your Go projects directory
```sh
$ git clone https://waushop@bitbucket.org/waushop/enlabs.git
```
#### Navigate into project's directory
```sh
$ cd enlabs
```
#### Run following commands
```sh
$ go mod init
```
```sh
$ go mod tidy
```
```sh
$ go get
```

#### Create .env file into project's directory
```sh
$ nano .env
```
#### and set following variables:
```sh
POSTGRES_HOST=enlabs-postgres                      
#POSTGRES_HOST=postgres host e.g. 127.0.0.1  # when running the app without docker
POSTGRES_USER=postgres username
POSTGRES_PWD=postgres password
POSTGRES_DB=enlabs
```
### Usage with 🐳
#### Run enlabs app and postgres in docker

```sh
$ docker-compose up
```
#### Build docker image
```sh
$ docker build -t enlabs .
```

### Usage without 🐳
#### Run
```sh
$ go run *.go
```
#### Build
```sh
$ go build
```
#### Run built executable
```sh
$ ./enlabs
```
## 🔍 Available endpoints
- #### GET /users - Get all users

- #### GET /users/1 - Get single user by ID
- #### POST /users/1/update - Send post request for single user by ID using form-data.
#### Required fields:
- status : 'win' or 'lost'
- amount : any amount in numbers e.g. '10.15'

## 📚 Project stack
- [golang.org](https://golang.org)
- [postgresql.org](https://www.postgresql.org)
- [docker.com](https://www.docker.com)
- [labstack/echo](https://github.com/labstack/echo)
- [gorm.io/gorm](https://gorm.io/gorm)
- [joho/godotenv](github.com/joho/godotenv)
- [mileusna/crontab](github.com/mileusna/crontab)

## 🔗 Live demo
- ### [enlabs.vaus.ee](http://enlabs.vaus.ee)

## 🔧 Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## 🎓 License
[MIT](https://choosealicense.com/licenses/mit/)